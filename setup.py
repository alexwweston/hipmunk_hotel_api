#!/usr/bin/env python

from distutils.core import setup

setup(name='hotel_api',
      version='1.0',
      description='Hotel search aggregator api',
      author='Alex Weston',
      author_email='alexwweston@gmail.com',
      packages=['hotel_search'],
      install_requires = [
          "flask>=1.0.1",
          "Flask-aiohttp>=0.1.0",
          "aiohttp==1.3.5"
      ],
      dependency_links=[
          "https://github.com/Hardtack/Flask-aiohttp/tarball/master#egg=Flask-aiohttp-0.1.0"
        ]
      )
    #   git+ssh://git@github.com/mwilliamson/mayo.git
