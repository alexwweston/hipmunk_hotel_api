"""Search Api

aggregates search results from external hotel apis
"""

from flask import Flask
import json
from os.path import dirname, join
from urllib import request
import asyncio
import aiohttp
from flask_aiohttp import AioHTTP, async

# set up the Flask app
app = Flask(__name__)

# make it async with flask_aiohttp
aio = AioHTTP(app)

# read in the config data
try:
    config_path = join(dirname(dirname(__file__)), 'config.json')
    config_f = open(config_path, 'r')
    search_config = json.load(config_f)
except FileNotFoundError as e:
    print("ERROR: could not find config file at: {}".format(config_path))
    raise e

@app.route('/')
def welcome():
    return 'Welcome to the hipmunk hotel search api! Use the /hotels/search endpoint to view all hotels'


@app.route('/hotels/search')
@async
def search():
    urls = search_config["external_hotel_search_apis"]

    results = yield from collect_search_results(urls)
    resp = {
        "results": results
    }
    return json.dumps(resp)

async def collect_search_results(urls):
    """ Launch async calls to external search apis and sort results
    Parameters
    ----------
    urls: list
        list of external urls
    Returns
    -------
    list
        sorted list of search results
    """
    # create a list async tasks that query external apis
    tasks = [asyncio.ensure_future(
        search_external_api(url)) for url in urls]
    # collect the responses from these tasks
    results = await asyncio.gather(*tasks)
    # flatten the results
    results = [ item for sublist in results for item in sublist ]
    results.sort(key=lambda x: x['ecstasy'], reverse=True)
    return results

async def search_external_api(url):
    """ Asynchronously get search results from a single external api
    Parameters
    ----------
    url: str
        external search api
    Returns
    -------
    list
        search results for individual external api
    """
    # use aiohttp to make an async request to 
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            data = await resp.text()
            return json.loads(data)['results']


if __name__ == '__main__':
    aio.run(app, port=8000)
