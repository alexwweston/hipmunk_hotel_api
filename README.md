# Hipmunk Hotel Search API

## Quickstart
- Set up and activate python 3 virtual environment
- cd into the project directory
- run the following:
```
pip install --process-dependency-links -e .
python -m hotel_search.hotel_search_api
```

## Notes
In retrospect, I would not have chosen Flask if I had known it wasn't built to support async (or I would rewrite it using a different library if I had more time to spare!). The flask_aiohttp makes async programming in Flask possible, but the author admits it's more of an experiment than a solid solution suitable for production. Next time, I'll be looking into Tornado or a different framework!

## Future directions and Improvement Ideas
- add error handling and messaging
